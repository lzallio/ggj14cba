/**
 * Control input management library
 */
#ifndef __CONTROLS_LIB
#define __CONTROLS_LIB

import "mod_key";
import "mod_joy";
import "mod_mem";
import "mod_string";
import "mod_file";

// default control action constants
const
CONTROL_OK = 1;
CONTROL_CANCEL = 2;
CONTROL_LEFT = 3;
CONTROL_RIGHT = 4;
CONTROL_UP = 5;
CONTROL_DOWN = 6;
end

global
	char controls_scancodes[98] = "E1234567890MPBTQWERTYUIOPLRECASDFGHJKLSAWLBZXCVBNMCPSCRCPASCFFFFFFFFFFNSHCUCPCCLCCRCCECDCPCICDCFFL";
end

type controls_buttonmap
	int control = 0; // the control action we want to check
	int player = 0; // the player number (when multiplayer)
	int value = 0; // the key id in the device (keyboard key, joy button, etc)	
	int device_id = -1; // when more than one device conected (joysticks)
	int axisnumber = -1; // when joystick
	int hatnumber = -1; // when joystick
end

global
// controls_defaultjoymap[5] = 9,8,JOY_HAT_LEFT,JOY_HAT_RIGHT,JOY_HAT_UP,JOY_HAT_DOWN;
int controls_mappings_keyboard_count = 0;
controls_buttonmap pointer controls_mappings_keyboard; // list of mapped buttons to the keyboard
int controls_mappings_joystick_count = 0;
controls_buttonmap pointer controls_mappings_joystick; // list of mapped buttons to the joysticks
end


/**
 * Loads buttons configuration from file
 */
function control_load(STRING filepath);
Private
    handle;   // handle for the loaded file 
	int i;
	
	_size;
	controls_buttonmap mappedcontrol_;
Begin
    handle=fopen(filepath,O_READ); // opens the file in writing mode
	
	// keyboard
    fread(handle,controls_mappings_keyboard_count); // reads the quantity of keyb keys assigned
	if (controls_mappings_keyboard_count == 0)
		free(controls_mappings_keyboard); // libera mem del puntero viejo
		controls_mappings_keyboard = null;
	else
		// redimencionar tamaño del puntero
		_size = (controls_mappings_keyboard_count) *  sizeof(controls_buttonmap);	
		
		controls_mappings_keyboard = realloc (controls_mappings_keyboard, _size);
		
		// loop the list of keyb keys assigned
		for (i = 0; i < controls_mappings_keyboard_count;i++)
			fread(handle,mappedcontrol_); // writes each one of the keys assigned
			
			controls_mappings_keyboard[i].control = mappedcontrol_.control;
			controls_mappings_keyboard[i].device_id = mappedcontrol_.device_id;
			controls_mappings_keyboard[i].player = mappedcontrol_.player;
			controls_mappings_keyboard[i].axisnumber = mappedcontrol_.axisnumber;
			controls_mappings_keyboard[i].hatnumber = mappedcontrol_.hatnumber;
			controls_mappings_keyboard[i].value = mappedcontrol_.value;
		end
	end
	
	// joystick
	fread(handle,controls_mappings_joystick_count); // writes the quantity of joy buttons assigned	
	if (controls_mappings_joystick_count == 0)
		free(controls_mappings_joystick); // libera mem del puntero viejo
		controls_mappings_joystick = null;
	else
		// redimencionar tamaño del puntero
		_size = (controls_mappings_joystick_count) *  sizeof(controls_buttonmap);	
		controls_mappings_joystick = realloc (controls_mappings_joystick, _size);
		
		// loop the list of joy buttons assigned
		for (i = 0; i < controls_mappings_joystick_count;i++)
			fread(handle,mappedcontrol_); // writes each one of the keys assigned
			
			controls_mappings_joystick[i].control = mappedcontrol_.control;
			controls_mappings_joystick[i].device_id = mappedcontrol_.device_id;
			controls_mappings_joystick[i].player = mappedcontrol_.player;
			controls_mappings_joystick[i].axisnumber = mappedcontrol_.axisnumber;
			controls_mappings_joystick[i].hatnumber = mappedcontrol_.hatnumber;
			controls_mappings_joystick[i].value = mappedcontrol_.value;
		end
    end
	
	fclose(handle);                 // zipping up after business is done
End

/**
 * Saves the current buttons configuration
 */
function control_save(STRING filepath);
Private
    handle;   // handle for the loaded file 
	int i;
	controls_buttonmap mappedcontrol_;
Begin
    handle=fopen(filepath,O_WRITE); // opens the file in writing mode
	
    fwrite(handle,controls_mappings_keyboard_count); // writes the quantity of keyb keys assigned
	
	// loop the list of keyb keys assigned
	for (i = 0; i < controls_mappings_keyboard_count;i++) 
		mappedcontrol_ = controls_mappings_keyboard[i];		
		fwrite(handle,mappedcontrol_); // writes each one of the keys assigned
	end
	
	fwrite(handle,controls_mappings_joystick_count); // writes the quantity of joy buttons assigned
	
	// loop the list of joy buttons assigned
	for (i = 0; i < controls_mappings_joystick_count;i++) 
		mappedcontrol_ = controls_mappings_joystick[i];		
		fwrite(handle,mappedcontrol_); // writes each one of the keys assigned
	end
    
	fclose(handle);                 // zipping up after business is done
End


/**
 * Waits for an user to press a button and assign it to an action
 * returns:
 * true if a button was assigned
 * false if the button was cleared
 */
function control_listen_assign(player, button)
private
    i,j,value;
    bool assigned = false;
begin
	scan_code = 0; // reseteamos el input del teclado
	
	loop
	    if (assigned) break; end
		
		frame; // aca en teoria se escucha el input
		
		// si se aprieta cancel limpiamos el boton y salimos 
		if (control(0,CONTROL_CANCEL))
			controls_clear_button(player, button);
			assigned = false;
			break;
		end
		
		// si se aprieta una tecla la definimos para la accion
		if (scan_code!= 0)
			controls_map_keyboard(button,player, scan_code);			
			assigned = true;
			break;
		end
		
		for (i=0;i< joy_numjoysticks();i++)
		
		    if (assigned) break; end
		    
		    for (j=0;j < joy_numbuttons (i); j++)
		        if(joy_getbutton(i,j))
                    controls_map_joystick(button, player, i, j);
                    assigned =  true;
                    break;
                end
		    end
		    
		    if (assigned) break; end
		    
		    for (j=0;j < joy_numaxes (i); j++)
		        if(joy_getaxis(i,j)<-20000);
                    controls_map_joy_axis(button,  player, i, j, -20000);
                    assigned =  true;
                    break;
                end                
                if(joy_getaxis(i,j)>20000);
                    controls_map_joy_axis(button,  player, i, j, 20000);
                    assigned =  true;
                    break;
                end
		    end
		    
		    if (assigned) break; end
		    
		    for (j=0;j < joy_numhats(i); j++)
		        if(value=JOY_GETHAT(i,j))
                    controls_map_joy_hat(button, player, i, j, value);
                    assigned =  true;
                    break;
                end
		    end
		end
	end
	
	
	
	if (assigned )
	    while (control(player,button))
	        frame;
	    end
	end
	
	return assigned;
end

/**
 * Returns an string describing the devices buttons assigned to an action
 */
function string control_get_assigned(player, button)

private
int i;
controls_buttonmap pointer mappedcontrol_;
string description = "";
begin
	
	// busca una tecla configurada para el control
	for (i = 0; i < controls_mappings_keyboard_count;i++) 
		mappedcontrol_ = controls_mappings_keyboard[i];
		if (mappedcontrol_.control == button and 
			((player != 0 and mappedcontrol_.player == player) or (player == 0)))			
			
			description = description + "Keyb: " + controls_scancodes[(mappedcontrol_.value - 1)] + ". ";
		end
	end
	
	// busca una tecla configurada para el control
	for (i = 0; i < controls_mappings_joystick_count;i++) 
		mappedcontrol_ = controls_mappings_joystick[i];
		if ((mappedcontrol_.control == button or mappedcontrol_.control == (button  - 10000)) and 
			((player != 0 and mappedcontrol_.player == player) or (player == 0)) and mappedcontrol_.device_id > -1)
			
			if(mappedcontrol_.axisnumber > -1 and mappedcontrol_.control == button )
				description = description +  "Joy " + mappedcontrol_.device_id + ": Axis " +  mappedcontrol_.axisnumber + " " + mappedcontrol_.value + ". "; // axis
			elseif(mappedcontrol_.hatnumber > -1 and mappedcontrol_.control == (button  - 10000))
				description = description +  "Joy " + mappedcontrol_.device_id + ": Hat " +  mappedcontrol_.hatnumber + " " + mappedcontrol_.value + ". "; // hat
			else
			
				description = description +  "Joy " + mappedcontrol_.device_id + ": Button " + mappedcontrol_.value + ". "; // Button
			end
		end
	end
	
	return trim(description);
end

/**
 * Check for if a control button is pressed in any controller device
 */
function control(player, button)
private
	value;
begin
	// check keyboard
	if (controls_button_checkkeyb(player, button))
		return true;
	end
	// check joystick
    if ((value = controls_button_checkjoy(player, button)) != false )
		return value;
	end
	return false;
end

/**
 * Check the button is pressed and then released
 */ 
function control_(player, button)
private
    value, return_value = false;	
begin
    while((value = control(player, button)) != false)		
		return_value = value;
        frame; // limpiamos buffer de teclas 
    end

    return return_value;
end

/**
 * clones all controls to another action
 */
function controls_clone_button(player, button, button_destination)
private
int i;
controls_buttonmap pointer mappedcontrol_;

controls_buttonmap pointer new_controls_mappings_keyboard;
int new_controls_mappings_keyboard_count = 0;

controls_buttonmap pointer new_controls_mappings_joystick;
int new_controls_mappings_joystick_count = 0;

int _size = 0;
begin
    if (controls_mappings_keyboard != NULL)
		// busca una tecla configurada para el control
		new_controls_mappings_keyboard_count = controls_mappings_keyboard_count;
		for (i = 0; i < new_controls_mappings_keyboard_count;i++) 
			mappedcontrol_ = controls_mappings_keyboard[i];
			if (mappedcontrol_.control == button and 
				((player != 0 and mappedcontrol_.player == player) or (player == 0)))
				
				// calcula el nuevo tamaño
				_size = (controls_mappings_keyboard_count+1) *  sizeof(controls_buttonmap);
				
				
				// redimencionar tamaño del puntero
				controls_mappings_keyboard = realloc (controls_mappings_keyboard, _size);
				
				controls_mappings_keyboard_count++;
				
				// setear el nuevo elemento	
				controls_mappings_keyboard[controls_mappings_keyboard_count - 1].control = button_destination;
				controls_mappings_keyboard[controls_mappings_keyboard_count - 1].player = mappedcontrol_.player;
				controls_mappings_keyboard[controls_mappings_keyboard_count - 1].value = mappedcontrol_.value;
				
			else
				
			    continue; // si no es del player y el boton indicado no lo pasamos al nuevo puntero				
							
			end
		end
	end
	
	_size = 0;
	
	if (controls_mappings_joystick != NULL)
		// busca una tecla configurada para el control
		new_controls_mappings_joystick_count = controls_mappings_joystick_count;
		for (i = 0; i < new_controls_mappings_joystick_count;i++) 
			mappedcontrol_ = controls_mappings_joystick[i];
			if ((mappedcontrol_.control == button or mappedcontrol_.control == (button  - 10000)) and 
				((player != 0 and mappedcontrol_.player == player) or (player == 0)))
				
			
				// calcula el nuevo tamaño
				_size = (controls_mappings_joystick_count+1) *  sizeof(controls_buttonmap);
				
				
				// redimencionar tamaño del puntero
				controls_mappings_joystick = realloc (controls_mappings_joystick, _size);
				
				controls_mappings_joystick_count++;
				
				// setear el nuevo elemento	
				controls_mappings_joystick[controls_mappings_joystick_count - 1].control = button_destination;
				controls_mappings_joystick[controls_mappings_joystick_count - 1].player = mappedcontrol_.player;
				controls_mappings_joystick[controls_mappings_joystick_count - 1].value = mappedcontrol_.value;
				controls_mappings_joystick[controls_mappings_joystick_count - 1].device_id = mappedcontrol_.device_id;
				controls_mappings_joystick[controls_mappings_joystick_count - 1].axisnumber = mappedcontrol_.axisnumber;
				controls_mappings_joystick[controls_mappings_joystick_count - 1].hatnumber = mappedcontrol_.hatnumber;
			else
			    continue;
			end
		end	
	end
	
	
	return;

end


/**
 * Removes all controls associated to an action
 */
function controls_clear_button(player, button)
private
int i;
controls_buttonmap pointer mappedcontrol_;

controls_buttonmap pointer new_controls_mappings_keyboard;
int new_controls_mappings_keyboard_count = 0;

controls_buttonmap pointer new_controls_mappings_joystick;
int new_controls_mappings_joystick_count = 0;

int _size = 0;
begin
	if (controls_mappings_keyboard != NULL)
		// busca una tecla configurada para el control
		for (i = 0; i < controls_mappings_keyboard_count;i++) 
			mappedcontrol_ = controls_mappings_keyboard[i];
			if (mappedcontrol_.control == button and 
				((player != 0 and mappedcontrol_.player == player) or (player == 0)))
				
				continue; // si es del player y el boton indicado no lo pasamos al nuevo puntero				
			else	
				// calcula el nuevo tamaño
				if ((new_controls_mappings_keyboard) == NULL)
					_size = sizeof(controls_buttonmap);
				else
					_size = (new_controls_mappings_keyboard_count+1) *  sizeof(controls_buttonmap);
				end
				
				// redimencionar tamaño del puntero
				new_controls_mappings_keyboard = realloc (new_controls_mappings_keyboard, _size);
				
				new_controls_mappings_keyboard_count++;
				
				// setear el nuevo elemento	
				new_controls_mappings_keyboard[new_controls_mappings_keyboard_count - 1].control = mappedcontrol_.control;
				new_controls_mappings_keyboard[new_controls_mappings_keyboard_count - 1].player = mappedcontrol_.player;
				new_controls_mappings_keyboard[new_controls_mappings_keyboard_count - 1].value = mappedcontrol_.value;				
			end
		end	
		
		free(controls_mappings_keyboard); // libera mem del puntero viejo
		
		controls_mappings_keyboard = new_controls_mappings_keyboard;
		controls_mappings_keyboard_count = new_controls_mappings_keyboard_count;		
	end
	
	_size = 0;
	
	if (controls_mappings_joystick != NULL)
		// busca una tecla configurada para el control
		for (i = 0; i < controls_mappings_joystick_count;i++) 
			mappedcontrol_ = controls_mappings_joystick[i];
			if ((mappedcontrol_.control == button or mappedcontrol_.control == (button  - 10000)) and 
				((player != 0 and mappedcontrol_.player == player) or (player == 0)))
				
				continue; // si es del player y el boton indicado no lo pasamos al nuevo puntero				
			else	
				// calcula el nuevo tamaño
				if ((new_controls_mappings_joystick) == NULL)
					_size = sizeof(controls_buttonmap);
				else
					_size = (new_controls_mappings_joystick_count+1) *  sizeof(controls_buttonmap);
				end
				
				// redimencionar tamaño del puntero
				new_controls_mappings_joystick = realloc (new_controls_mappings_joystick, _size);
				
				new_controls_mappings_joystick_count++;
				
				// setear el nuevo elemento	
				new_controls_mappings_joystick[new_controls_mappings_joystick_count - 1].control = mappedcontrol_.control;
				new_controls_mappings_joystick[new_controls_mappings_joystick_count - 1].player = mappedcontrol_.player;
				new_controls_mappings_joystick[new_controls_mappings_joystick_count - 1].value = mappedcontrol_.value;
				new_controls_mappings_joystick[new_controls_mappings_joystick_count - 1].device_id = mappedcontrol_.device_id;
				new_controls_mappings_joystick[new_controls_mappings_joystick_count - 1].axisnumber = mappedcontrol_.axisnumber;
				new_controls_mappings_joystick[new_controls_mappings_joystick_count - 1].hatnumber = mappedcontrol_.hatnumber;
			end
		end	
		
		free(controls_mappings_joystick); // libera mem del puntero viejo
		
		controls_mappings_joystick = new_controls_mappings_joystick;
		controls_mappings_joystick_count = new_controls_mappings_joystick_count;		
	end
	
	
	return;
end

/**
 * Maps a keyboard scancode to a game control action.
 */
function controls_map_keyboard(int control,int player, int scancode)
private
int i;
controls_buttonmap pointer mappedcontrol_;
int _size = 0;
begin
	// calcular el tamaño nuevo del puntero.
	if ((controls_mappings_keyboard) == NULL)
		_size = sizeof(controls_buttonmap);
	else
		
		// buscar que no exista already
		for (i = 0; i < controls_mappings_keyboard_count;i++) 
			mappedcontrol_ = controls_mappings_keyboard[i];
			if (mappedcontrol_.control == control and 
				mappedcontrol_.player == player and 
				mappedcontrol_.value == scancode)			
				
				return; // duplicated
			end
		end		
		// calcula el nuevo tamaño
		_size = (controls_mappings_keyboard_count+1) *  sizeof(controls_buttonmap);
	end
	
	// redimencionar tamaño del puntero
	controls_mappings_keyboard = realloc (controls_mappings_keyboard, _size);
	
	controls_mappings_keyboard_count++;
	
	// setear el nuevo elemento	
	controls_mappings_keyboard[i].control = control;
	controls_mappings_keyboard[i].player = player;
	controls_mappings_keyboard[i].value = scancode;
end
/**
 * Maps a joystick axis to an action
 */
function controls_map_joy_hat(int control, int player, int joystick_id, int hatnumber, int value)
private
int i;
controls_buttonmap pointer mappedcontrol_;
int _size = 0;
begin
	// calcular el tamaño nuevo del puntero.
	if ((controls_mappings_joystick) == NULL)
		_size = sizeof(controls_buttonmap);
	else
		
		// buscar que no exista already
		for (i = 0; i < controls_mappings_joystick_count;i++) 
			mappedcontrol_ = controls_mappings_joystick[i];
			if (mappedcontrol_.control == (control  - 10000)  and 
				mappedcontrol_.player == player and 
				mappedcontrol_.hatnumber == hatnumber and
				mappedcontrol_.value == value )
				
				return; // duplicated
			end
		end		
		// calcula el nuevo tamaño
		_size = (controls_mappings_joystick_count+1) *  sizeof(controls_buttonmap);
	end
	
	// redimencionar tamaño del puntero
	controls_mappings_joystick = realloc (controls_mappings_joystick, _size);
	
	controls_mappings_joystick_count++;
	
	// setear el nuevo elemento	
	controls_mappings_joystick[i].control = (control  - 10000);
	controls_mappings_joystick[i].device_id = joystick_id;
	controls_mappings_joystick[i].player = player;
	controls_mappings_joystick[i].hatnumber = hatnumber;
	controls_mappings_joystick[i].axisnumber = -1;
	controls_mappings_joystick[i].value = value ;
end
/**
 * Maps a joystick axis to an action
 */
function controls_map_joy_axis(int control, int player, int joystick_id, int axisnumber, int value)
private
int i;
controls_buttonmap pointer mappedcontrol_;
int _size = 0;
begin
	// calcular el tamaño nuevo del puntero.
	if ((controls_mappings_joystick) == NULL)
		_size = sizeof(controls_buttonmap);
	else
		
		// buscar que no exista already
		for (i = 0; i < controls_mappings_joystick_count;i++) 
			mappedcontrol_ = controls_mappings_joystick[i];
			if (mappedcontrol_.control == control and 
				mappedcontrol_.player == player and 
				mappedcontrol_.axisnumber == axisnumber and
				mappedcontrol_.value == value)
				
				return; // duplicated
			end
		end		
		// calcula el nuevo tamaño
		_size = (controls_mappings_joystick_count+1) *  sizeof(controls_buttonmap);
	end
	
	// redimencionar tamaño del puntero
	controls_mappings_joystick = realloc (controls_mappings_joystick, _size);
	
	controls_mappings_joystick_count++;
	
	// setear el nuevo elemento	
	controls_mappings_joystick[i].control = control;
	controls_mappings_joystick[i].device_id = joystick_id;
	controls_mappings_joystick[i].player = player;
	controls_mappings_joystick[i].axisnumber = axisnumber;
	controls_mappings_joystick[i].hatnumber = -1;
	controls_mappings_joystick[i].value = value;
end

/**
 * Maps a joystick button to a game control action.
 */
function controls_map_joystick(int control, int player, int joystick_id, int button)
private
int i;
controls_buttonmap pointer mappedcontrol_;
int _size = 0;
begin
	// calcular el tamaño nuevo del puntero.
	if ((controls_mappings_joystick) == NULL)
		_size = sizeof(controls_buttonmap);
	else
		
		// buscar que no exista already
		for (i = 0; i < controls_mappings_joystick_count;i++) 
			mappedcontrol_ = controls_mappings_joystick[i];
			if (mappedcontrol_.control == control and 
				mappedcontrol_.player == player and 
				mappedcontrol_.device_id == joystick_id and 
				mappedcontrol_.value == button)
				
				return; // duplicated
			end
		end		
		// calcula el nuevo tamaño
		_size = (controls_mappings_joystick_count+1) *  sizeof(controls_buttonmap);
	end
	
	// redimencionar tamaño del puntero
	controls_mappings_joystick = realloc (controls_mappings_joystick, _size);
	
	controls_mappings_joystick_count++;
	
	// setear el nuevo elemento	
	controls_mappings_joystick[i].control = control;
	controls_mappings_joystick[i].device_id = joystick_id;
	controls_mappings_joystick[i].player = player;
	controls_mappings_joystick[i].value = button;
	controls_mappings_joystick[i].axisnumber = -1;
	controls_mappings_joystick[i].hatnumber = -1;
end


 
/**
 * Check if a button is pressed via the keyboard handler
 */
function controls_button_checkkeyb(player, button)
private
int i;
controls_buttonmap pointer mappedcontrol_;
begin
	// busca una tecla configurada para el control
	for (i = 0; i < controls_mappings_keyboard_count;i++) 
		mappedcontrol_ = controls_mappings_keyboard[i];
		if (mappedcontrol_.control == button and 
			((player != 0 and mappedcontrol_.player == player) or (player == 0)))			
			
			if (key(mappedcontrol_.value))
				return true;
			end
		end
	end
	
	return false;
end

/**
 * returns true if a button on any device binded to an action is pressed. 
 * if the device button is an axis it will return the value of the axis
 */
function controls_button_checkjoy(player, button)
private
int i;
controls_buttonmap pointer mappedcontrol_;
value;
begin
	// busca una tecla configurada para el control
	for (i = 0; i < controls_mappings_joystick_count;i++) 
		mappedcontrol_ = controls_mappings_joystick[i];
		
		if ((mappedcontrol_.control == button or mappedcontrol_.control == (button  - 10000)) and 
			((player != 0 and mappedcontrol_.player == player) or (player == 0)) and mappedcontrol_.device_id > -1)
			
			// check joystick
			
			
			if(mappedcontrol_.axisnumber > -1 and mappedcontrol_.control == button )
				if (mappedcontrol_.value > 0)
					if((value = joy_getaxis(mappedcontrol_.device_id,mappedcontrol_.axisnumber)) > mappedcontrol_.value)
						return value;
					end
				else
					if((value = joy_getaxis(mappedcontrol_.device_id,mappedcontrol_.axisnumber)) < mappedcontrol_.value)
						return value;
					end
				end
			end
			if(mappedcontrol_.hatnumber > -1 and mappedcontrol_.control == (button  - 10000))
				// HAT check
				if (joy_gethat(mappedcontrol_.device_id ,0) == mappedcontrol_.value)
					return true;
				end
			end
			if (joy_getbutton(mappedcontrol_.device_id ,mappedcontrol_.value) and mappedcontrol_.control == button)
				return true;
			end
			
			// •  •  •  •  •  •  •  • 
			
		end
	end
	
	return false;
end





#endif
