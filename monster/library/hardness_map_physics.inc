import "mod_map";
import "mod_draw";
include "../library/math.inc";
global
	// pointer to the hardness map
	hmp_hardness_map_file = 0; 
	hmp_hardness_map_graph; 
	
	// configuration of the global physics that affects all procceses
	hmp_gravity = 1;	
	hmp_max_vertical_speed = 12;
end

local
	// physics vars of each proccees
	
	// hardness map collision box
	hmp_box_height = 0;
	hmp_box_width = 0;
	
	// process speed
	hmp_inc_x = 0; //velocidad horizontal
	hmp_inc_y = 0; //velocidad vertical
	
	// saves the las color checked on each boundary
	int hmp_collision_bottom = 0; //colision abajo
	int hmp_collision_top = 0; //colision arriba
	int hmp_collision_left = 0; //colision izquierda
	int hmp_collision_right = 0; //colision derecha		
	
	// physics properties
	float hmp_bounce_rate = 0; // how much bounces percentage (from zero to one, ex: 0.2 = 20%)
	
	// misc
	hmp_y_prev = 0;
end

function check_collision_bottom(process_id, float p_x, float p_y)
private
	i = 0, j = 0;
	map_height, map_width;	
begin
	map_height = map_info(hmp_hardness_map_file,hmp_hardness_map_graph,G_HEIGHT); 
	map_width = map_info(hmp_hardness_map_file,hmp_hardness_map_graph,G_WIDTH);
	
	for (i = p_x - (process_id.hmp_box_width / 8); i <= p_x + (process_id.hmp_box_width / 8); i++)
		for (j = (p_y); j <= p_y + (process_id.hmp_box_height/2); j++)
			if (i > 0 and j>0 and i<map_width and j<map_height)
				hmp_collision_bottom = map_get_pixel(hmp_hardness_map_file,hmp_hardness_map_graph,i,j);
				if (hmp_collision_bottom>0)
					return hmp_collision_bottom;
				end
			end
		end		
	end
	return 0;
end

function check_collision_top(process_id, float p_x, float p_y)
private
	i = 0, j = 0;
	map_height, map_width;	
begin
	map_height = map_info(hmp_hardness_map_file,hmp_hardness_map_graph,G_HEIGHT); 
	map_width = map_info(hmp_hardness_map_file,hmp_hardness_map_graph,G_WIDTH);
	
	for (i = p_x - (process_id.hmp_box_width / 8) ; i <= p_x + (process_id.hmp_box_width / 8) ; i++)
		for (j = (p_y); j >= p_y - (process_id.hmp_box_height/2); j--)
			if (i > 0 and j>0 and i<map_width and j<map_height)
				hmp_collision_top = map_get_pixel(hmp_hardness_map_file,hmp_hardness_map_graph,i,j);
				if (hmp_collision_top>0)
					return hmp_collision_top;
				end		
			end
		end
	end
	return 0;
end

function check_collision_left(process_id, float p_x, float p_y)
private
	i = 0, j = 0;
	map_height, map_width;	
begin
	map_height = map_info(hmp_hardness_map_file,hmp_hardness_map_graph,G_HEIGHT); 
	map_width = map_info(hmp_hardness_map_file,hmp_hardness_map_graph,G_WIDTH);
	
	for (i = p_y - (process_id.hmp_box_height / 8) ; i <= p_y + (process_id.hmp_box_height / 8) ; i++)
		for (j = (p_x); j >= p_x - (process_id.hmp_box_width/2); j--)
			if (i > 0 and j>0 and j<map_width and i<map_height)				
				hmp_collision_left = map_get_pixel(hmp_hardness_map_file,hmp_hardness_map_graph,j,i);				
				if (hmp_collision_left>0)
					return hmp_collision_left;
				end
			end
		end
	end
	return 0;
end

function check_collision_right(process_id, float p_x, float p_y)
private
	i = 0, j = 0;
	map_height, map_width;	
begin
	map_height = map_info(hmp_hardness_map_file,hmp_hardness_map_graph,G_HEIGHT); 
	map_width = map_info(hmp_hardness_map_file,hmp_hardness_map_graph,G_WIDTH);
	
	for (i = p_y - (process_id.hmp_box_height / 8);  i <= p_y + (process_id.hmp_box_height / 8); i++)
		for (j = (p_x); j <= p_x + (process_id.hmp_box_width/2); j++)
			if (i > 0 and j>0 and j<map_width and i<map_height)
				hmp_collision_right = map_get_pixel(hmp_hardness_map_file,hmp_hardness_map_graph,j,i);
				if (hmp_collision_right>0)
					return hmp_collision_right;
				end		
			end
		end
	end
	return 0;
end

import "mod_say";
// applies physics to the process who called it. Last only 1 frame
process hardness_map_physics()
private
	i = 0, j = 0;
	advance_step, advance_distance;
	
	float inc_x,inc_y,p_x,p_y;
	
	float value_inc_x, value_inc_y;
	
	
begin
	// Gravity	
	if ((father.hmp_inc_y + hmp_gravity) < hmp_max_vertical_speed)
		father.hmp_inc_y += hmp_gravity;
	else
		father.hmp_inc_y = hmp_max_vertical_speed;
	end
	
	// actualizamos posicion
	p_x = father.x;
	p_y = father.y;
	
	value_inc_x = (float) abs(father.hmp_inc_x);
	value_inc_y = (float) abs(father.hmp_inc_y);
	
	// calculamos avance pixel por pixel
	if (value_inc_x >= value_inc_y and value_inc_x>0)
		inc_x = 1; 
	else 
		inc_x = (value_inc_x/value_inc_y);
	end
	if (value_inc_y >= value_inc_x and value_inc_y>0)
		inc_y = 1 ; 
	else
		inc_y = (value_inc_y/value_inc_x);
	end
	
	
	// le damos el signo
	if (father.hmp_inc_x<0) inc_x *= -1; end
	if (father.hmp_inc_y<0) inc_y *= -1; end
	
	// avanzamos pixel por pixel calculando que no nos choquemos con nadina
	advance_step = 0;
	if (value_inc_x >= value_inc_y )
		advance_distance = value_inc_x;
	else
		advance_distance = value_inc_y;
	end
	
	while( advance_step < advance_distance)
		
		// avanzamos de a un pixel aca
		p_x += inc_x;
		p_y += inc_y;
		
		hmp_collision_bottom = check_collision_bottom(father, round(p_x), round(p_y) +1 );
		hmp_collision_top = check_collision_top(father, round(p_x), round(p_y)-1);
		hmp_collision_left = check_collision_left(father, round(p_x)-1, round(p_y));
		hmp_collision_right = check_collision_right(father, round(p_x)+1, round(p_y));
		
		
		// autoajuste de dureza		
		if (hmp_collision_bottom > 0)
			if (father.hmp_box_height == 0 or father.hmp_box_width == 0)
				p_y -= 0.5;
			else	
				while(check_collision_bottom(father, round(p_x), round(p_y))>0);				
					p_y = (p_y - 1);
				end
			end
		end		
		if (hmp_collision_top > 0)
			if (father.hmp_box_height == 0 or father.hmp_box_width == 0)
				p_y += 0;
			else	
				while(check_collision_top(father, round(p_x), round(p_y))>0);
					p_y = (p_y + 1);
				end
			end
		end		
		if (hmp_collision_left > 0)
			if (father.hmp_box_height == 0 or father.hmp_box_width == 0)
				p_x += 0.6;
			else	
				while(check_collision_left(father, round(p_x), round(p_y))>0);
					p_x = (p_x + 1);
				end
			end
		end		
		if (hmp_collision_right > 0)
			if (father.hmp_box_height == 0 or father.hmp_box_width == 0)
				p_x -= 0.6;
			else	
				while(check_collision_right(father, round(p_x), round(p_y))>0);
					p_x = (p_x - 1);
				end
			end
		end		
		
		advance_step ++;
	end
	
	
	
	// actualizamos banderas de colision
	father.hmp_collision_bottom = hmp_collision_bottom;
	father.hmp_collision_top = hmp_collision_top;
	father.hmp_collision_left = hmp_collision_left;
	father.hmp_collision_right = hmp_collision_right;
	
	// actualizamos posicion del padre
	father.x = round(p_x);
	father.y = round(p_y);

	// rebote
	if (father.hmp_collision_bottom>0 or father.hmp_collision_top>0)	
		father.hmp_inc_y = round(father.hmp_inc_y * father.hmp_bounce_rate) * -1; // bouncing
	end
	
	if (father.hmp_collision_left>0 or father.hmp_collision_right>0)	
		father.hmp_inc_x = round(father.hmp_inc_x * father.hmp_bounce_rate) * -1; // bouncing
	end

	
end