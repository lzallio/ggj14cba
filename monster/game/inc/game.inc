process game()
private
	player_id;
	stage_visible;
	scroll_id;
	i = 0;
begin
	fade_on();
	hmp_hardness_map_graph = create_stage();
	police_1 = 0;
	police_2 = 0;
	stage_visible = new_map(4000,360,16);
	drawing_map(0,stage_visible);
	map_clear(0,stage_visible,RGB(255,0,0));
	drawing_color(RGB(0,0,0));
	draw_box(0,203,4000,360);

	scroll_id = start_scroll(0,0,stage_visible,0,0,0);
	player_id = player_process();
	pill();
	scroll[0].camera = player_id;

	loop
		if(game_ending > 0 and game_ending < 20)
			fade_off();
			game_ending++;
		elseif(game_ending >= 20)
			if(bad_monsters == 1)
				police_1 = hooman_process(1,0,3600,300);
				hooman_process(1,0,3620,300);
				police_2 = hooman_process(1,0,3550,300);
				hooman_process(1,1,3450,300);
				hooman_process(0,1,3500,300);
				hooman_process(1,1,3380,300);
				hooman_process(1,1,3400,300);
				hooman_process(0,1,3750,300);
				hooman_process(0,1,3850,300);
				hooman_process(0,0,3800,300);
				text_sign(RGB(0,1,60),"FREEZE!", 60);
			end
			signal(type enemy_process, S_KILL);
			signal(type bullet, S_KILL);		
			fade_on();
			drawing_map(0,stage_visible);
			drawing_color(RGB(200,200,200));
			draw_box(0,0,4000,202);
			game_ending = 0;
		end
		if((force_game_over))
			fade_off();
			force_game_over = false;
			bad_monsters = 0;
			game_ending = 0;
			allow_spawns = 1;
			while(i < 20)
				i++;
				frame;
			end
			i = 0;
			return;
		end;
		frame;
	end
	onexit:
		delete_text(ALL_TEXT);
		stop_scroll(0);
end

process start()
private
	this_txt;
begin
	set_text_color(RGB(255,0,0));
	this_txt = write(0,320,150,4,'M O N S T E R');
	force_game_over = false;
	bad_monsters = 0;
	game_ending = 0;
	allow_spawns = 1;
	map_put(0,background,tutorial,310,250);
	fade_on();
	loop
		frame;
	end
	onexit:
		delete_text(ALL_TEXT);
		clear_screen();
end

process fin()
private
	this_txt;
begin
	set_text_color(RGB(255,255,255));
	this_txt = write(0,320,150,4,'fin');
	fade_on();
	loop
		frame;
	end
	onexit:
		delete_text(ALL_TEXT);
end
