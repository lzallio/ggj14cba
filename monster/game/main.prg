import "mod_time"
import "mod_map";
import "mod_grproc";
import "mod_key";
import "mod_wm";
import "mod_video";
import "mod_scroll";
import "mod_mouse";
import "mod_proc";
import "mod_draw";
import "mod_rand";
import "mod_math";
import "mod_timers";
import "mod_text";
import "mod_screen";

include "../library/controls.inc";
include "../library/hardness_map_physics.inc";

global
	main_fpg;
	bad_monsters = 0;
	game_ending = 0;
	allow_spawns = 1;
	current_status;
	bool force_game_over = false;
	hooman_process police_1, police_2;
	tutorial;
End;
const
	CONTROL_JUMP = 17;
	CONTROL_SHOT = 18;
end

include "inc/player.inc";
include "inc/stages.inc";
include "inc/game.inc";


Process Main()
private
	dsx, dsy;
	i;
Begin
	// config de la fisica
	hmp_gravity = 1;
	hmp_max_vertical_speed = 12;
	tutorial = load_png('inc/tut.png');
	rand_seed(time()); // para que el random sea random y no siempre lo mesmo
	
	main_fpg = load_fpg('inc/g.fpg');
	
	// configuracion de pantalla
	set_fps(30,0);
	// escalado de pantalla
	//scale_resolution=13600768;
	//full_screen = true;
	set_mode(640,360,16);

	// info de debug
	//write(0,5,20,0,"FPS:");
	//write_int(0,40,20,0,&fps);
	
	// configuracion inicial
	include "inc/defaultcontrols.inc";
		
	// configuramos la partida
	// corre el juego
	loop
		let_me_alone();
		current_status = start();
		while(exists(current_status))
			if (key(_ESC))
				exit();
			end
			if (control_(1,CONTROL_SHOT))
				signal(current_status,S_KILL_TREE);
			end
			frame;
		end
		let_me_alone();
		current_status = game();
		while(exists(current_status))
			if (key(_ESC))
				exit();
			end
			frame;
		end
		let_me_alone();
		current_status = fin();
		while(exists(current_status))
			if (control_(1,CONTROL_SHOT))
				signal(current_status,S_KILL_TREE);
			end
			frame;
		end
	end

End
