global
	players[1];
end


process game()
private
stage1,stage2,i;
item;
side;
acid_process;
modal;
begin
	stage1 = create_stage(1);
	stage2 = create_stage(2);
	jump_wav = load_wav("res/ogg/jump_01.wav");
	powerup_wav = load_wav("res/ogg/pildora_01.wav");
	powerup1_wav = load_wav("res/ogg/pildora_02.wav");
	powerup2_wav = load_wav("res/ogg/pildora_03.wav");
	sticky_wav = load_wav("res/ogg/sticky_jump.wav");
	sticky_land_wav = load_wav("res/ogg/sticky_land.wav");
	inflate_wav = load_wav("res/ogg/balloon_01.wav");
	fart_wav = load_wav("res/ogg/sopla1.wav");
	fart1_wav = load_wav("res/ogg/sopla2.wav");
	fart2_wav = load_wav("res/ogg/sopla3.wav");
	water_wav = load_wav("res/ogg/agua_fondo.wav");
	youwin_wav = load_wav("res/ogg/you_win.wav");
	play_wav(water_wav,-1);
	
	for (i=0;i<2; i++)
		players[i] = player(i+1);
		players[i].y = stage_height - 50;
		spawn(players[i],i+1); // ubica al personaje en el lugar mas conveniente
	end
	
	acid_process = acid();
	
	loop
		if (rand(1,500)<5)
			if (rand(0,100)<=50)
				side = 1;
			else
				side = 2;
			end
			item = morph_item(side );
			item.y = players[side-1].y-150; 
			spawn(item,side);
		end
		
		for (i=0;i<2; i++)
			if (players[i].y >= acid_process.y)
				
				modal = you_win_dialog(i);
				while(exists(modal))
					frame;
				end
			end
		end
		frame;
		if (key(_ESC))
			exit();
		end
	end
	onexit:
	signal(id,s_kill_tree); 
end

process winner_cartel()
begin
	file = uifpg;
	graph = 7;
	loop
		frame;
	end
end

process carteludo(int player_id)
begin
	file = uifpg;
	graph = 4 + player_id;
	y = 180;
	x = 320;
	if (player_id==1)
		x -=60;
	else
		x +=60;
	end
	loop
		frame;
	end
end 

process you_win_dialog(looser)
private
map;
cartele1,cartele2;
agrandau;
winner;
	begin
		play_wav(youwin_wav,0);
		signal(ALL_PROCESS,s_freeze); 
		cartele1 = carteludo(1);
		cartele1.size=60;
		cartele2 = carteludo(2);
		cartele2.size=60;
		if (looser==1)
			agrandau = cartele1;
		else
			agrandau = cartele2;
		end
		
		while (agrandau.size<120)
		agrandau.size+=5;
		agrandau.z--;
		frame;
		end
		
		winner = winner_cartel();
		winner.x = agrandau.x;
		winner.y = 60;
		winner.z = agrandau.z-10;
		winner.size = 30;
		
		while (winner.size<120)
		winner.size+=5;
		
		frame;
		end
			
		while(!control_(0,CONTROL_OK) )
			frame;
		end		
onexit:	
	signal(ALL_PROCESS,s_wakeup); 
	signal(father,s_kill_tree); 
end


process morph_item(half_id)
private
map;
player catcher;
string kinds[2] = "sticky","balloon", "asteroid";
string new_kind;
time_to_kill_item;
begin
	ctype = c_scroll;
	if(half_id==1)
		cnumber = C_1;
	else
		cnumber = C_2;
	end
	hmp_box_height = 20;
	hmp_box_width = 20;
	// Create a new graph of size 100x100 and color depth of 8bit
    //map = map_new(20,20,16);
	hmp_bounce_rate=0.9;
    
    // Clear the map red
    map_clear(0,map,rgb(255,255,255));

    //graph = map;
		file = char_fpg;
		graph = 86;
	time_to_kill_item = timer[0];
	loop
		// kill item if time_to_kill is >  1500 then kill item
		if(timer[0]-time_to_kill_item > 1500)
			//signal(new_kind, S_KILL);
			return;
		end

		hardness_map_physics(hmp_hardness_map_file[half_id-1],hmp_hardness_map_graph[half_id-1]);	
		angle -=15000;
		if (catcher = collision(type player))
			if (catcher == players[half_id-1])
			new_kind = catcher.kind;
			repeat
				new_kind = kinds[rand(0,2)];
			until(new_kind != catcher.kind)
			catcher.kind = new_kind;
			if(new_kind!="balloon")
				if(timer[0]%2==0)
					play_wav(powerup_wav,0);
				elseif(timer[0]%3==0)	
					play_wav(powerup1_wav,0);
				else	
					play_wav(powerup2_wav,0);
				end	
			else
				play_wav(inflate_wav,0);
			end	
				
			catcher.status = "standing";
			catcher.energy=0;
			catcher.angle = 0;
			catcher.flags=0;
			break;
			end
						
		end
        frame;
    end
	
	while (alpha>0)
		angle -=30000;
		alpha-=10;
		y--;
		frame;
	end
	
	onexit:
	
	unload_map(0,map);
end

process acid()
private
map;
saved_timer;
catcher;
modal;
anim[23]=1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,8,8,8;
i=0;

begin
	saved_timer = timer[0];
	x=160;
	z=-1;
	y=stage_height-30;
	
	file = acido_fpg;
	alpha=200;
	ctype = c_scroll;
    acid_effect();
	loop
		graph = anim[i];
		i++;
		
		if (i == sizeof(anim)/sizeof(int))
			i=0;
		end
		
		y-= 1 * (timer[0]-saved_timer)/3000;
        frame;
		
    end
	
	onexit:
	
	
end

process acid_effect()
private
map;

begin

map = new_map(320,1,16);
map_clear(0,map,rgb(57,255,153));
graph=map;
ctype = c_scroll;
set_center(0,map,160,0);
priority = father.priority -1;
loop
	y = father.y+49;
	z = father.z -1;
	x = father.x;
	alpha = father.alpha;
	
	size =abs( y - stage_height)*100;
	frame;
end
onexit:
unload_map(0,map);
end



process menu()
private
	start_text, credits_text, lo_credito;
	selected = 0;
begin
	x=320;
	y=180;
	
	selected = 0;
	file = uifpg;
	graph = 8;
	set_text_color(RGB(255,0,0));
	start_text = write(0,295,280,0,"START <");
	set_text_color(RGB(0,0,0));
	credits_text = write(0,295,300,0,"CREDITS");
	loop
		if(control_(0,CONTROL_DOWN) or control_(0,CONTROL_UP) and selected <> 2)
			delete_text(start_text);
			delete_text(credits_text);
			if(selected == 0)
				set_text_color(RGB(0,0,0));
				start_text = write(0,295,280,0,"START");
				set_text_color(RGB(255,0,0));
				credits_text = write(0,295,300,0,"CREDITS <");
				selected = 1;
			else
				set_text_color(RGB(255,0,0));
				start_text = write(0,295,280,0,"START <");
				set_text_color(RGB(0,0,0));
				credits_text = write(0,295,300,0,"CREDITS");
				selected = 0;
			end
		end
		if(control_(0,CONTROL_OK))
			if(selected == 0)
				delete_text(start_text);
				delete_text(credits_text);
				return;
			elseif(selected == 1)
				delete_text(start_text);
				delete_text(credits_text);
				lo_credito = credits();
				selected = 2;
			else
				signal(lo_credito,S_KILL);
				set_text_color(RGB(255,255,255));
				start_text = write(0,295,270,0,"START");
				set_text_color(RGB(255,200,50));
				credits_text = write(0,295,280,0,"CREDITS <");
				selected = 1;
			end
		end

		frame;
	end
end

process credits()
private
begin
	while(!key(_ESC))
		size_x=100;
		size_y=100;
		x=320;
		y=180;
		z=-10;
		
		file = uifpg;
		graph = 4;
		frame;
		
	end	

end

