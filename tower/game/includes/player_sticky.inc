		last_y  = y;
		hardness_map_physics(hmp_hardness_map_file[player_number-1],hmp_hardness_map_graph[player_number-1]);		
		
		
		
		// Motor de estados
		switch(status):
			
			case "standing":
				
				if(control(player_number,CONTROL_JUMP))
					
					if(control(player_number,CONTROL_UP))
						hmp_inc_y = jump_height-2;
					elseif(control(player_number,CONTROL_DOWN) and !hmp_collision_bottom)
						hmp_inc_y = jump_height *-1;
					else
						hmp_inc_y = jump_height;
					end
					
					if(control(player_number,CONTROL_LEFT) or hmp_collision_right)
						hmp_inc_x = (char_max_speed*3) *-1;
					end
					
					if((control(player_number,CONTROL_RIGHT) or hmp_collision_left))
						hmp_inc_x = (char_max_speed*3);
					end
					
					status= "jumping";
					if (hmp_collision_bottom)
						set_anim(skin,&anim_sticky_jump,sizeof(anim_sticky_jump),false);
					else
						set_anim(skin,&anim_sticky_wall_jump,sizeof(anim_sticky_wall_jump),false);
					end
				else
					
					if(energy == 0 and !hmp_collision_top and !hmp_collision_bottom and !hmp_collision_right and !hmp_collision_left)
						status= "jumping";
						set_anim(skin,&anim_sticky_jump,sizeof(anim_sticky_jump),false);
					end
					
						
					hmp_inc_x = 0;
					hmp_inc_y = 0;
					y = last_y;
						
					
						if ( energy >=2)
							kind="normal";
							angle=0;
							status="standing";		
							energy=0;
						end
					
					
					if (skin.anim_pointer != &anim_sticky_wall and skin.anim_pointer != &anim_sticky_standing)
						set_anim(skin,&anim_sticky_standing,sizeof(anim_sticky_standing),false);
					end
					
				end
				
			end
			case "jumping":
					if(hmp_collision_top || hmp_collision_bottom || hmp_collision_right || hmp_collision_left
						)
						hmp_inc_x = 0;
						hmp_inc_y = 0;
						y = last_y;
						
						if(timer[0]%2==0)
							play_wav(sticky_wav,0);
						else
							play_wav(sticky_land_wav,0);
						end	
						
						if (hmp_collision_bottom )
							energy++;
						end
						if(hmp_collision_top or hmp_collision_bottom)
							
							set_anim(skin,&anim_sticky_standing,sizeof(anim_sticky_standing),false);
							
							if (hmp_collision_top)
								flags = 2;
							else
								flags = 0;
							end
						else
							if (hmp_collision_left)
								flags = 1;
							else
								flags = 0;
							end
							
							set_anim(skin,&anim_sticky_wall,sizeof(anim_sticky_wall),false);
							
						end
						status ='standing';
					end
					
			end
			case "dying":				
				dying_time -= 1;
				
				if(dying_time <= 0 )					
					break; // la muerte del proceso ocurre aqui
				end
			end
		end
		

