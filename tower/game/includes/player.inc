local 
	// vars usadas por la animacion	
	byte pointer anim_pointer = null;
	byte anim_count;
	byte anim_pos = 0;
	boolean anim_looper = 0;
end
global
// animaciones
	byte anim_standing[9] = 66,66,67,67,68,68,69,69,70,70;
	byte anim_run[5] = 2,3,4,5,6,7;
	byte anim_hit[3] = 8,8,9,9;
	byte anim_jump[4] = 10,11,12,13,14;
	byte anim_falling[2] = 15,16,17;
	byte anim_pared[3] = 18,19,20,21;
	
	byte anim_balloon_standing[7] = 22,23,24,25,26,27,28,29;
	byte anim_balloon_hit[3]=30,30,31,31;
	
	byte anim_sticky_standing[9]=32,33,34,35,36,37,38,39,40,41;
	byte anim_sticky_jump[9]=42,43,44,45,46,47,48,49,50,51;
	byte anim_sticky_wall[6]=52,53,54,55,56,57,58;
	byte anim_sticky_wall_jump[6]=59,60,61,62,63,64,65;
	
	byte anim_pedo_standing[13]=72,72,73,73,74,74,75,75,76,76,77,77,78,78;
	byte anim_pedo_run[20]=79,79,79,80,80,80,81,81,81,82,82,82,83,83,83,84,84,84,85,85,85;
end

function set_anim(process_id,byte pointer anim_pointer,count,boolean looper)
begin	
	process_id.anim_pos = 0;
	process_id.anim_pointer = anim_pointer;
	process_id.anim_count = count;	
	process_id.anim_looper = looper;	
end

process player_skin()
begin
	ctype = c_scroll;
	cnumber = father.cnumber;
	priority = father.priority - 1;
	file = char_fpg;
	while (exists(father))
		flags = father.flags;
		//alpha = father.alpha;
		x = father.x;
		y = father.y-10;
		angle = father.angle;
		
		if (anim_pointer != null)
			// Cambia el frame de animaci�n 
			anim_pos++;
			
			if (anim_pos >= anim_count)
				if (anim_looper)
					anim_pos = 0;
				else
					anim_pos = anim_count -1;
				end
			end	
			
			graph = anim_pointer[anim_pos]; // Muestra el grafico correspondiente
		end
		
		frame;
	end
end

Process player(int player_number)
public 
	string status = "standing";


	string kind = "normal";
	energy=0; // because We need use in game.inc
private
//	i = 0;
//	j = 0;
	skin;
	int initial_flag = 0;
	bool on_air = false;
	// config de comportamiento
	jump_height = -12;
	char_speed = 0;
	char_max_speed = 5;
	pressed_jump;
	dying_time;
	speed=1;
	last_timer;
	last_y;
	dirt;
Begin
	// crea grafico de colision 
	file = 0;		
	graph = new_map(25,25,16);	
	map_clear(0,graph,rgb(255,0,0));
	//set_center(0,graph,8,10);
	alpha = 0;
	
	ctype = c_scroll;
	if(player_number==1)
		cnumber = C_1;
	else
		cnumber = C_2;
	end
	
	skin = player_skin();
	
	scroll[player_number].camera=id;
	
	// tama�o del personaje para el chequeo de durezas
	hmp_box_height = 25;
	hmp_box_width = 25;
	
	playernumber = player_number -1;	
	

	loop
		switch (kind)
			case "normal":
				include "includes/player_normal.inc";
			end
			case "balloon":
				include "includes/player_baloon.inc";
			end
			case "sticky":
				include "includes/player_sticky.inc";
			end
			case "asteroid":
				include "includes/player_asteroid.inc";
			end
		end	
		frame;
	end
	//onexit:
	//	unload_map(0,graph);
End

process wall_dirt()
private
anim[3]=1,2,3,4;
i;
begin
	file = fx_fpg;
	ctype = c_scroll;
	cnumber = father.cnumber;
	x = father.x;
	y=father.y;
	flags = father.flags;
	z = father.z-1;
	loop
		graph = anim[i];
		i++;
		
		if (i > sizeof(anim)/sizeof(int))
			return;
		end
		frame;
	end
end

process spawn(process_id,half_id)
private
map;
i,j;
color_check;
begin
	signal(process_id, s_sleep);
	ctype = c_scroll;
	if(half_id==1)
		cnumber = C_1;
	else
		cnumber = C_2;
	end
	repeat
		process_id.x = rand(0,639);
		process_id.y = rand(process_id.y-50,process_id.y+50);
		color_check = 0;
		for (i=process_id.x-process_id.hmp_box_width/2; i <= process_id.x+process_id.hmp_box_width/2; i++)
			for (j=process_id.y-process_id.hmp_box_height/2; j <= process_id.y+process_id.hmp_box_height/2; j++)
				color_check = map_get_pixel(hmp_hardness_map_file[half_id-1],hmp_hardness_map_graph[half_id-1],i,j);
				if (color_check!=0)
					break;
				end
			end
			if (color_check!=0)
				break;
			end
		end
	until(color_check == 0);
		
	
	// Create a new graph of size 100x100 and color depth of 8bit
    map = map_new(100,100,16);
    
    // Clear the map red
    map_clear(0,map,rgb(130,130,130));

    graph = map;
	x = process_id.x;
	y = process_id.y;
	flags = 16;

    Repeat
		size-=10;
		angle -=15000;
        frame;
    Until(size<=0)
	
	onexit:
	signal(process_id, s_wakeup);
	unload_map(0,map);
end
