switch(os_id)
	default:
		// player 1
		
		controls_map_joy_hat(CONTROL_UP, 1, 0, 0,JOY_HAT_UP);
		controls_map_joy_hat(CONTROL_DOWN, 1, 0, 0,JOY_HAT_DOWN);
		controls_map_joy_hat(CONTROL_LEFT, 1, 0, 0,JOY_HAT_LEFT);
		controls_map_joy_hat(CONTROL_RIGHT, 1, 0, 0,JOY_HAT_RIGHT);
		
		controls_map_joy_axis(CONTROL_UP, 1, 0, 1, -10000);
		controls_map_joy_axis(CONTROL_DOWN, 1, 0, 1, 10000);
		controls_map_joy_axis(CONTROL_LEFT, 1, 0, 0, -10000);
		controls_map_joy_axis(CONTROL_RIGHT, 1, 0, 0, 10000);
		
		controls_map_joystick(CONTROL_JUMP,1, 0, 0); // boton A xbox gamepad
		controls_map_joystick(CONTROL_JUMP,1, 0, 1); // boton B xbox gamepad
		
		controls_map_joystick(CONTROL_OK,1, 0, 1); // boton B xbox gamepad
		
		controls_map_keyboard(CONTROL_LEFT,1, _a);
		controls_map_keyboard(CONTROL_RIGHT,1, _d);
		controls_map_keyboard(CONTROL_DOWN,1, _s);
		
		controls_map_keyboard(CONTROL_JUMP,1, _v);
		controls_map_keyboard(CONTROL_UP,1, _w);
		// player 2
		controls_map_keyboard(CONTROL_LEFT,2, _left);
		controls_map_keyboard(CONTROL_RIGHT,2, _right);
		controls_map_keyboard(CONTROL_DOWN,2, _down);
		
		controls_map_keyboard(CONTROL_JUMP,2, _CONTROL);
		controls_map_keyboard(CONTROL_UP,2, _up);
		controls_map_keyboard(CONTROL_OK,2, _enter);	

		controls_map_joy_hat(CONTROL_UP, 2, 1, 0,JOY_HAT_UP);
		controls_map_joy_hat(CONTROL_DOWN, 2, 1, 0,JOY_HAT_DOWN);
		controls_map_joy_hat(CONTROL_LEFT, 2, 1, 0,JOY_HAT_LEFT);
		controls_map_joy_hat(CONTROL_RIGHT, 2, 1, 0,JOY_HAT_RIGHT);
		
		controls_map_joy_axis(CONTROL_UP, 2, 1, 1, -10000);
		controls_map_joy_axis(CONTROL_DOWN, 2, 1, 1, 10000);
		controls_map_joy_axis(CONTROL_LEFT, 2, 1, 0, -10000);
		controls_map_joy_axis(CONTROL_RIGHT, 2, 1, 0, 10000);
		
		controls_map_joystick(CONTROL_JUMP,2, 1, 0); // boton A xbox gamepad
		controls_map_joystick(CONTROL_JUMP,2, 1, 1); // boton B xbox gamepad
		
		controls_map_joystick(CONTROL_OK,2, 1, 1); // boton B xbox gamepad
	end
end