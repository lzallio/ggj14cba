global
	
	
end

global
		shape_width = 6; // tamano menos 2
	shape_height = 5; // tamano menos 2
	shapes_count = 6; // cantidad de shapes que tenemos en el array
	
	// array de formas de 7x6 (entran 4 de largo y 3 de alto contando los border mergeables)
	
	
	unsigned byte stage_shapes[335] = 		
	// forma vacia
		1, 3, 2, 0, 0, 0, 0, 0, 
		5,15,10, 0, 0, 0, 0, 0,
		4, 12,8, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
	// // forma vacia
		0, 0, 0, 0, 1, 3, 3, 2,
		0, 0, 0, 0, 5,15,15,10,		
		0, 0, 0, 0, 4,12,12, 8,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
	// // forma llena 1
		1, 2, 0, 0, 0, 0, 0, 0,
		5,10, 0, 0, 0, 0, 0, 0,
		5,10, 0, 0, 0, 0, 0, 0,
		4, 8, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 1, 3, 2,
		0, 0, 0, 0, 0, 5,15,10,		
		0, 0, 0, 0, 0, 4,12, 8,
		// // forma llena 2
		0, 0, 0, 0, 0, 0, 0, 0,		
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		1, 3, 3, 2, 0, 0, 0, 0,
		5,15,15,10, 0, 0, 0, 0,
		4,12, 12,8, 0, 0, 0, 0,
		// // forma llena 3		
		0, 0, 0, 0, 1, 3, 3, 2,
		0, 0, 0, 0, 5,15,15,10, 		
		0, 0, 0, 0, 4,12,12, 8,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		// // forma llena 4		
		1, 3, 2, 0, 0, 0, 0, 0,
		5,15,10, 0, 0, 0, 0, 0,
 		4,12, 8, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1, 3, 3, 2,
		0, 0, 0, 0, 4,12,12, 8;
end


/**
 * Generates a random stage array and saves it in the pointer argument
 */
function create_random_stage( byte pointer stage, p_stage_width, p_stage_height)
private
i, j, k, l;
random_shape;
unsigned byte new_tile, current_tile, result_tile;
string line = "";
begin
	
	// vaciamos la pantalla
	For(i=0;i<(p_stage_width* p_stage_height);i++)
		stage[i] = 0;
	End
	
	// creamos borde
	For(j=0; j<p_stage_height; j++)
		For(i=0; i<p_stage_width;i++)
			if ( j==0 or i==0 or i==p_stage_width-1 or j==p_stage_height-1 )
				stage[(j * p_stage_width ) + (i)] = 15;
			end
		end
	end
	
	For(j=1; j<p_stage_height-1; j++)
		For(i=1; i<p_stage_width-1;i++)
			if ( j==1 and i==1 )
				stage[(j * p_stage_width ) + (i)] = 14;
			elseif( j==p_stage_height-2 and i==1 )
				stage[(j * p_stage_width ) + (i)] = 11;			
			elseif ( j==1 and i==p_stage_width-2 )
				stage[(j * p_stage_width ) + (i)] = 13;
			elseif ( j==p_stage_height-2 and i==p_stage_width-2 )
				stage[(j * p_stage_width ) + (i)] = 7;
			elseif ( j>1 and i==1 )
				stage[(j * p_stage_width ) + (i)] = 10;
			elseif ( j==1 and i>1 )
				stage[(j * p_stage_width ) + (i)] = 12;
			elseif ( j==p_stage_height-2 and i>1 )
				stage[(j * p_stage_width ) + (i)] = 3;
			elseif ( j>1 and i==p_stage_width-2 )
				stage[(j * p_stage_width ) + (i)] = 5;				
			end
		end
	end
	
	// creamos contenido random

	For(j=1  ;j<p_stage_height ;j+=(shape_height+1))
		For(i=1  ;i<p_stage_width ;i+=(shape_width+1))		

			// elegimos una forma a pintar
			random_shape = rand(1,shapes_count) - 1;
			
			for (l = -1; l < shape_height + 1; l++)
				for (k = -1 ; k < shape_width + 1; k++)
					if (i+k >= 0 and i+k < p_stage_width
					and j+l >= 0 and j+l < p_stage_height)
						// sumamos al tile actual el valor que esta en la forma en esa posicion
					
						current_tile = stage[((j+l) * p_stage_width ) + (i+k)];
						new_tile = stage_shapes[( (random_shape * ((shape_width+2) * (shape_height+2)))  + (((l+1) * (shape_width+2)) + (k+1)))];
							
						
							// descomponemos el nuevo tile
							result_tile = current_tile;
							
							switch (new_tile):
								case 3:
									result_tile |= 1;
									result_tile |= 2;
								end
								case 5:
									result_tile |= 1;
									result_tile |= 4;
								end
								case 10:
									result_tile |= 2;
									result_tile |= 8;
								end
								case 12:
									result_tile |= 4;
									result_tile |= 8;
								end
								case 7:
									result_tile |= 1;
									result_tile |= 4;
									result_tile |= 2;
								end
								case 11:
									result_tile |= 2;
									result_tile |= 8;
									result_tile |= 1;
								end
								case 13:
									result_tile |= 1;
									result_tile |= 4;
									result_tile |= 8;
								end
								case 14:
									result_tile |= 2;
									result_tile |= 8;
									result_tile |= 4;
								end
								default:
									result_tile |= new_tile;
								end
							end
						
							
						
						stage[((j+l) * p_stage_width ) + (i+k)] = result_tile;
					end
				end
			end	
			
		End
	End
	/*
	For(j=0;j<p_stage_height;j++)	
		line = "";
		For(i=0;i<p_stage_width;i++)	
			if (stage[(j * p_stage_width ) + i]  <10)
			line += " " + stage[(j * p_stage_width ) + i] + ",";
			else
			line += stage[(j * p_stage_width ) + i] + ",";
			end
		End
		say(line);
	End
	
	exit();
	*/
end
/**
 * creates a hardness map and returns it
 */ 
function create_stage_hardness_map( byte pointer stage, p_stage_width, p_stage_height)
private
i,j;
tile_size;
string line;
new_map;
begin
	tile_size = (map_info ( hardness_tiles_fpg , 1 , G_WIDTH ));
	
	new_map = map_new ( tile_size * p_stage_width , tile_size * p_stage_height , 16, B_CLEAR);
	
	For(j=0;j< p_stage_height;j++)
		For(i=0;i< p_stage_width;i++)
			if (stage[(j * p_stage_width ) + i] > 0)
				map_xputnp ( 0 , new_map, hardness_tiles_fpg , stage[(j * p_stage_width ) + i] , i * tile_size + (tile_size/2), j * tile_size + (tile_size/2), 0 , 100 , 100 , 0 );
			end
		End		
	End
	
	return new_map;
end
/**
 * creates a tiled map and returns it
 */ 
function create_stage_skin_map( byte pointer stage, p_stage_width, p_stage_height)
private
i,j;
tile_size;
string line;
new_map;
begin
	tile_size = (map_info ( theme_tiles_fpg , 1 , G_WIDTH ));
	
	new_map = map_new ( tile_size * p_stage_width , tile_size * p_stage_height , 16, B_CLEAR);
	
	For(j=0;j< p_stage_height;j++)
		For(i=0;i< p_stage_width;i++)
			if (stage[(j * p_stage_width ) + i] > 0)
				map_xputnp ( 0 , new_map, theme_tiles_fpg , stage[(j * p_stage_width ) + i] , i * tile_size + (tile_size/2), j * tile_size + (tile_size/2), 0 , 100 , 100 , 0 );
			end
		End		
	End
	
	return new_map;
end

process create_stage(half_id)
private
	
	int background_map;
	min_x,max_x,min_y,max_y;
	
	player player_id;
	size_1, size_2;
	alto_pantalla,ancho_pantalla;
	new_width,new_height;
	
	min_zoom_offset = 70;
	zoom_easing = 10;
	
	new_size,new_center_x,new_center_y,center_x,center_y;	
	
	float zoom_step, zoom;
	
	stage_render_map;
	stage_render_source;
	stage_render_proccess;
	stage_darkness_effect;
	
	
	byte pointer stage;
	stage_tile_width, stage_tile_height;
begin
	stage_tile_width = 16;
	stage_tile_height = 500;
	
	
	stage = alloc( (stage_tile_width*stage_tile_height) * sizeof( byte)); // alloc memory to store map array
	
	create_random_stage(stage, stage_tile_width, stage_tile_height);
	
	hmp_hardness_map_file[half_id-1] = 0;
	hmp_hardness_map_graph[half_id-1] = create_stage_hardness_map(stage, stage_tile_width, stage_tile_height);
	
	
	stage_width = graphic_info ( hmp_hardness_map_file[half_id-1],hmp_hardness_map_graph[half_id-1] , G_WIDTH );
	stage_height = graphic_info ( hmp_hardness_map_file[half_id-1],hmp_hardness_map_graph[half_id-1] , G_height );
	
	center_set(hmp_hardness_map_file[half_id-1],hmp_hardness_map_graph[half_id-1],0,0);
	
	stage_render_map = new_map(stage_width, stage_height,16);
	
	stage_render_source = create_stage_skin_map(stage, stage_tile_width, stage_tile_height);
	
	
	
	// tomamos tamano de la resolucion
	ancho_pantalla = graphic_info ( 0,BACKGROUND , G_width );
	alto_pantalla = graphic_info ( 0,BACKGROUND , G_height );
	
	if (scale_mode != SCALE_NONE)
		ancho_pantalla = ancho_pantalla / 2;
		alto_pantalla = alto_pantalla / 2;
	end
	
	// definimos las region
	region_define (half_id, (ancho_pantalla/2) * (half_id -1),0 ,(ancho_pantalla/2) , alto_pantalla );
	
	
	// definimos scroll
	start_scroll(half_id,0,stage_render_source,background_png,half_id,8);
	
	// seteamos posicion inicial de cada scroll
	Scroll[half_id].x0 = (ancho_pantalla/2) * (half_id -1);
	Scroll[half_id].y0 = stage_height;
	
	loop
		frame;
	end
	
	onexit:
	
	stop_scroll(half_id);
	unload_map(0,stage_render_map);
	unload_map(0,stage_render_source);	
	signal(id, s_kill_tree); // mapa el proceso que muestra el scroll
end
