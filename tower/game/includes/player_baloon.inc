	
		hardness_map_physics(hmp_hardness_map_file[player_number-1],hmp_hardness_map_graph[player_number-1]);		

		if(!control(player_number,CONTROL_JUMP) and status == "jumping")
			pressed_jump = false;	// para permitir rebote en pared
		end

		hmp_inc_y= -2;
		
		// Motor de estados
		switch(status):
			case "running":
					if (skin.anim_pointer != &anim_balloon_standing)
						set_anim(skin,&anim_balloon_standing,sizeof(anim_balloon_standing),true);
					end
					// corre
					char_speed = char_max_speed;
					if((hmp_collision_left == RGB(0,0,255) and hmp_inc_x < 0) or (hmp_collision_right == RGB(0,0,255) and hmp_inc_x > 0))
						char_speed -= 2;
						hmp_inc_y -= 1;
					end
					if(control(player_number,CONTROL_RIGHT))
						if(hmp_inc_x < char_speed)
							hmp_inc_x++;
						end
						flags = initial_flag;
					elseif(control(player_number,CONTROL_LEFT))
						flags = initial_flag + 1;
						if(hmp_inc_x > (char_speed*(-1)))
							hmp_inc_x--;
						end
					end
					if(hmp_collision_bottom or hmp_collision_right or hmp_collision_left or hmp_collision_top )
						status="dying";
						
						set_anim(skin,&anim_balloon_hit,sizeof(anim_balloon_hit),false);
					
					end
			end
			case "standing":
				if (skin.anim_pointer != &anim_balloon_standing)
						set_anim(skin,&anim_balloon_standing,sizeof(anim_balloon_standing),true);
					end
				if(control(player_number,CONTROL_RIGHT) or control(player_number,CONTROL_LEFT)) 
					// cambia a running
					status = "running";
					//set_anim(legs,&anim_running_legs,sizeof(anim_running_legs));
				else // standing
					if(hmp_inc_x > 0) // frenando para un lado
						
						if (hmp_inc_x>=2)
							hmp_inc_x-=2;
						else
							hmp_inc_x--;
						end
						
						//set_anim(legs,&anim_slowing_legs,sizeof(anim_slowing_legs));
						
					elseif(hmp_inc_x < 0) // frenando pal otro
						
						if (hmp_inc_x<=-2)
							hmp_inc_x+=2;
						else
							hmp_inc_x++;
						end
			
						//set_anim(legs,&anim_standing_legs,sizeof(anim_standing_legs));						
					end
					if(hmp_collision_bottom or hmp_collision_right or hmp_collision_left or hmp_collision_top )
						energy++;
						if(energy>5)
							status="dying";
								set_anim(skin,&anim_balloon_hit,sizeof(anim_balloon_hit),false);
						end	
					end
				end
			end
			case "dying":				
				
				if (skin.anim_pos==skin.anim_count-1)
				kind="normal";
				angle=0;
				status="standing";		
				energy=0;				
				end
			end
		end