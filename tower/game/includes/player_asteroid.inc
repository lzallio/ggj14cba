	
		hardness_map_physics(hmp_hardness_map_file[player_number-1],hmp_hardness_map_graph[player_number-1]);		

		if(!control(player_number,CONTROL_JUMP) and status == "jumping")
			pressed_jump = false;	// para permitir rebote en pared
		end
		
	
		// Motor de estados
		switch(status):
			case "standing":
				if (speed == 0)
					if (skin.anim_pointer != &anim_pedo_standing)
						set_anim(skin,&anim_pedo_standing,sizeof(anim_pedo_standing),true);
					end
				else
					if (skin.anim_pointer != &anim_pedo_run)
						set_anim(skin,&anim_pedo_run,sizeof(anim_pedo_run),true);
						if(timer[0]%2==0)
							play_wav(fart2_wav,0);
						elseif(timer[0]%3==0)
							play_wav(fart1_wav,0);
						else
							play_wav(fart_wav,0);
						end
					end
				end
				
				if(control(player_number,CONTROL_DOWN) or control(player_number,CONTROL_JUMP))
					if(speed<5)
						speed++;
					end	
					hmp_inc_y = cos(angle) * -speed;
					hmp_inc_x = sin(angle) * -speed;
					last_timer = timer[0];
				elseif(timer[0]-last_timer >60)
						if(hmp_inc_y > -1)
							hmp_inc_y--;
						end
						if(hmp_inc_y < -1)
							hmp_inc_y++;
						end
						if(hmp_inc_x > 0)
							hmp_inc_x--;
						end
						if(hmp_inc_x < 0)
							hmp_inc_x++;
						end


					if(speed>0 && timer[0]%2==0)
						speed--;
					end
				else
					hmp_inc_y--;

				end	
				if(hmp_collision_bottom or hmp_collision_top or hmp_collision_right or hmp_collision_left)
						if(hmp_collision_bottom)
							hmp_inc_y = -4;
						end
						if(hmp_collision_top)
							hmp_inc_y = 4;
						end
						if(hmp_collision_left)
							hmp_inc_x = 4;
						end
						if(hmp_collision_right)
							hmp_inc_x = -4;
						end
						
						energy++;
						if(energy >4)
							status = "dying";
						end
				end
				
				if(control(player_number,CONTROL_RIGHT) or control(player_number,CONTROL_LEFT)) 
					// cambia a running
					if(control(player_number,CONTROL_RIGHT))
						angle-=4000;
					else
						angle+=4000;
					end
				end
			end
			case "dying":
				status = "standing";			
				angle=0;
				kind = "normal";
			end
		end
