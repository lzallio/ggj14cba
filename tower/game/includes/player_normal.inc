	
		hardness_map_physics(hmp_hardness_map_file[player_number-1],hmp_hardness_map_graph[player_number-1]);		

		if(!control(player_number,CONTROL_JUMP) and (status == "standing" or status == "running") and hmp_inc_y == 0)
			pressed_jump = 0;
			hmp_inc_y++; // para que hace esto? IDKLOL
		elseif(control(player_number,CONTROL_JUMP) and hmp_inc_y > 5)
			pressed_jump = 1;
			
		elseif(!control(player_number,CONTROL_JUMP) and (status == "standing" or status == "running" ) and hmp_inc_y > 3)	
			// si se esta en el aire cambiamos el estado a jumping
			status = "jumping";
			set_anim(skin,&anim_jump,sizeof(anim_jump),false);

			on_air = true;
		end
		
		if(!control(player_number,CONTROL_JUMP) and status == "jumping")
			pressed_jump = false;	// para permitir rebote en pared
		end

		if(!control(player_number,CONTROL_JUMP) and status == "jumping")
			hmp_inc_y++;
		end
		
		
		
		// Motor de estados
		switch(status):
			case "running":
				if(control(player_number,CONTROL_JUMP) and status != "jumping" and pressed_jump == 0 and hmp_inc_y <= 0)
					// salta					
					play_wav(jump_wav,0);
					status = "jumping";
					set_anim(skin,&anim_jump,sizeof(anim_jump),false);	
					pressed_jump = 1;
					hmp_inc_y = jump_height;
					on_air = true;
				elseif(!control(player_number,CONTROL_RIGHT) and !control(player_number,CONTROL_LEFT))
					// frena
					status = "standing";
					
					set_anim(skin,&anim_standing,sizeof(anim_standing),false);
				else
					// corre
					char_speed = char_max_speed;
					if((hmp_collision_left == RGB(0,0,255) and hmp_inc_x < 0) or (hmp_collision_right == RGB(0,0,255) and hmp_inc_x > 0))
						char_speed -= 2;
						hmp_inc_y -= 1;
					end
					if(control(player_number,CONTROL_RIGHT))
						if(hmp_inc_x < char_speed)
							hmp_inc_x++;
						end
						flags = initial_flag;
					elseif(control(player_number,CONTROL_LEFT))
						flags = initial_flag + 1;
						if(hmp_inc_x > (char_speed*(-1)))
							hmp_inc_x--;
						end
					end
					
				end
			end
			case "standing":
				if(control(player_number,CONTROL_JUMP) and status != "jumping" and pressed_jump == 0 and hmp_inc_y <= 0)
					play_wav(jump_wav,0);
					// cambia a jumping
					status = "jumping";					
					set_anim(skin,&anim_jump,sizeof(anim_jump),false);					
					pressed_jump = 1;
					hmp_inc_y = jump_height;
					on_air = true;
					if(hmp_inc_x > 0)
						hmp_inc_x--;
					elseif(hmp_inc_x < 0)
						hmp_inc_x++;
					end
				elseif(control(player_number,CONTROL_RIGHT) or control(player_number,CONTROL_LEFT))
					// cambia a running
					if(control(player_number,CONTROL_JUMP))
						play_wav(jump_wav,0);
					end;
					status = "running";
					set_anim(skin,&anim_run,sizeof(anim_run),true);
				else // standing
					if(hmp_inc_x > 0) // frenando para un lado
						
						if (hmp_inc_x>=2)
							hmp_inc_x-=2;
						else
							hmp_inc_x--;
						end
						
						set_anim(skin,&anim_jump,sizeof(anim_jump),false);
						
					elseif(hmp_inc_x < 0) // frenando pal otro
						
						if (hmp_inc_x<=-2)
							hmp_inc_x+=2;
						else
							hmp_inc_x++;
						end
						
						set_anim(skin,&anim_jump,sizeof(anim_jump),false);
					else		
						if (skin.anim_pointer != &anim_standing and !(skin.anim_pointer == &anim_hit and skin.anim_pos <skin.anim_count-1) )
						set_anim(skin,&anim_standing,sizeof(anim_standing),true);
						end
					end
					if(hmp_collision_bottom == RGB(0,0,255))
						hmp_inc_y -= 1;
					end
				end
			end
			case "jumping":
				if(control(player_number,CONTROL_RIGHT) or control(player_number,CONTROL_LEFT))
					// controla direccion en el salto
					char_speed = char_max_speed;
					if(control(player_number,CONTROL_RIGHT))
						if(hmp_inc_x < char_speed)
							hmp_inc_x++;
						end
						flags = initial_flag;
					elseif(control(player_number,CONTROL_LEFT))
						flags = initial_flag + 1;
						if(hmp_inc_x > (char_speed*(-1)))
							hmp_inc_x--;
						end
					end
				end
				
				// codigo para rebotar en la pared
				if (( hmp_collision_right or  hmp_collision_left) and !hmp_collision_top)
					if (control(player_number,CONTROL_RIGHT) and hmp_collision_right
						or control(player_number,CONTROL_LEFT) and hmp_collision_left) 

						// frenando contra la pared
						if (hmp_inc_y>2)
							hmp_inc_y -= 2; 
						end
						if (skin.anim_pointer != &anim_pared)
							set_anim(skin,&anim_pared,sizeof(anim_pared),true);
						end
						
						if (!exists(dirt))
							dirt=wall_dirt();
						end
					end
					
					
					if (control(player_number,CONTROL_JUMP) and !pressed_jump)
						
						play_wav(jump_wav,0);
					
						pressed_jump = true;
						if (hmp_collision_right)
							//saltamos para la izquierda
							hmp_inc_x = -char_speed ;
						
						else
							//saltamos para la izquierda
						
							hmp_inc_x = char_speed ;
						end
						
						hmp_inc_y = jump_height;
					end
				
				elseif (hmp_inc_y>=0 and skin.anim_pointer != &anim_falling)
					set_anim(skin,&anim_falling,sizeof(anim_falling),false);
				elseif (hmp_inc_y<0 and skin.anim_pointer != &anim_jump)
					set_anim(skin,&anim_jump,sizeof(anim_jump),false);
						
				end
				
				if(hmp_collision_bottom != 0)
					// toca el suelo
					on_air = false;
					hmp_inc_y = 0;
					status = "standing";			
					set_anim(skin,&anim_hit,sizeof(anim_hit),false);
				end
				

			end
			case "dying":				
				dying_time -= 1;
				
				if(dying_time <= 0 )					
					break; // la muerte del proceso ocurre aqui
				end
			end
		end