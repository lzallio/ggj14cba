import "mod_time"
import "mod_map";
import "mod_grproc";
import "mod_key";

import "mod_wm";
import "mod_video";
import "mod_scroll";
import "mod_mouse";
import "mod_proc";
import "mod_draw";
import "mod_rand";
import "mod_math";
import "mod_timers";
import "mod_text";
import "mod_screen";
import "mod_sound"

include "../library/controls.inc";
include "../library/hardness_map_physics.inc";
include "../library/motion_tween.inc";
include "../library/math.inc";

global	

	hmp_hardness_map_file[1];
	hmp_hardness_map_graph[1];
	playernumber;
	uifpg;

	hardness_tiles_fpg,theme_tiles_fpg,char_fpg,acido_fpg,fx_fpg;
	stage_width,stage_height;
	
	background_png;
	
	jump_wav;
	powerup_wav;
	powerup1_wav;
	powerup2_wav;
	inflate_wav;	
	music_ogg;	
	sticky_wav;
	sticky_land_wav;
	fart_wav;
	fart1_wav;
	fart2_wav;
	water_wav;
	youwin_wav;
End
const
	CONTROL_JUMP = 17;
	CONTROL_ENTER = 17;
end

include "includes/player.inc";
include "includes/stages.inc";
include "includes/game.inc";


Process Main()
private
	dsx, dsy;
	i;
	current_status;
Begin
	// config de la fisica
	hmp_gravity = 1;
	hmp_max_vertical_speed = 12;
	
	rand_seed(time()); // para que el random sea random y no siempre lo mesmo
	
	// configuracion de pantalla
	full_screen = false;	
	set_fps(30,0);
	// escalado de pantalla
	scale_mode = SCALE_none;
	set_mode(640,360,16);

	// info de debug
	write(0,5,20,0,"FPS:");
	write_int(0,40,20,0,&fps);
	// carga de recursos
	hardness_tiles_fpg = load_fpg("res/fpg/tiles_hardness.fpg");
	theme_tiles_fpg = load_fpg("res/fpg/tiles_theme.fpg");
	char_fpg = load_fpg("res/fpg/char.fpg");
	acido_fpg = load_fpg("res/fpg/acido.fpg");

	background_png = load_png("res/png/background_3.png");
	music_ogg = load_song("res/ogg/aliencito_3_master.ogg");
	uifpg = load_fpg("res/fpg/ui.fpg");
	fx_fpg = load_fpg("res/fpg/fx.fpg");
	play_song(music_ogg,-1); // load sound principal
	// configuracion inicial
	include "includes/defaultcontrols.inc";
	loop
		let_me_alone();
		current_status = menu();
		while(exists(current_status))
			if (key(_ESC))
				exit();
			end
			frame;
		end
		let_me_alone();
		current_status = game();
		while(exists(current_status))
			if (key(_ESC))
				exit();
			end
			frame;
		end
		frame;
	end	
    
End
